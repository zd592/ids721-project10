import json
import os
import tempfile
from transformers import pipeline

# Set temporary directory for any cache from transformers
tempfile.tempdir = '/tmp'
os.environ['TRANSFORMERS_CACHE'] = '/tmp'

# Load the model using transformers' pipeline for text generation
model = pipeline("text-generation", model="openai-gpt")

def lambda_handler(event, context):
    """
    Handles incoming events by generating text based on the 'input_text' field of the event.
    
    Parameters:
    event (dict): The event dictionary containing the 'input_text'.
    context: Provides information about the invocation, function, and runtime environment.

    Returns:
    dict: A dictionary with statusCode and the generated text.
    """
    # Extract 'input_text' from the event, default to empty string if not found
    input_text = event.get("input_text", "")

    # Generate text based on the input
    generated_text = generate_text(input_text)

    # Return results in a JSON compatible format
    return {
        "statusCode": 200,
        "body": json.dumps({
            "generated_text": generated_text
        })
    }

def generate_text(input_text):
    """
    Generates text using a pretrained model from Hugging Face Transformers.

    Parameters:
    input_text (str): Initial text to seed the generation process.

    Returns:
    str: The generated text.
    """
    # Generate text with a maximum length of 100 tokens
    results = model(input_text, max_length=200, num_return_sequences=1)
    # Extract the generated text from the results
    generated_text = results[0]['generated_text']
    return generated_text
