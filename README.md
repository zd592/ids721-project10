## Prerequisites

- AWS CLI configured with the necessary access rights.
- Docker installed and running on your local machine.
- An AWS account with permissions to manage Lambda functions and ECR repositories.

## Steps

### 1. Build Your Docker Image

```bash
docker build -t myapp .
```

### 2. Authenticate Docker to Amazon ECR

```bash
aws ecr get-login-password --region <your-region> | docker login --username AWS --password-stdin <your-aws-account-id>.dkr.ecr.<your-region>.amazonaws.com
```

Replace `<your-region>` and `<your-aws-account-id>` with your AWS region and account ID, respectively.

### 3. Tag Your Docker Image

```bash
docker tag myapp:latest <your-aws-account-id>.dkr.ecr.<your-region>.amazonaws.com/my-lambda-repo:latest
```

### 4. Push the Image to Amazon ECR

```bash
docker push <your-aws-account-id>.dkr.ecr.<your-region>.amazonaws.com/my-lambda-repo:latest
```

### 5. Create the Lambda Function

```bash
aws lambda create-function --function-name my-lambda-function \
  --package-type Image \
  --code ImageUri=<your-aws-account-id>.dkr.ecr.<your-region>.amazonaws.com/my-lambda-repo:latest \
  --role arn:aws:iam::<your-aws-account-id>:role/LambdaExecutionRole \
  --timeout 15 --memory-size 512
```

Replace `<your-aws-account-id>`, `<your-region>`, and `LambdaExecutionRole` with the appropriate values.

### 6. Invoke the Lambda Function

```bash
aws lambda invoke --function-name my-lambda-function \
  --cli-binary-format raw-in-base64-out \
  --payload '{"input_text":"test input"}' response.json
```

## Cleanup

To avoid incurring charges, remove the resources when they are no longer needed.

### Delete the Lambda Function

```bash
aws lambda delete-function --function-name my-lambda-function
```

### Delete the ECR Image

```bash
aws ecr batch-delete-image --repository-name my-lambda-repo --image-ids imageTag=latest
```

### Delete the ECR Repository

```bash
aws ecr delete-repository --repository-name my-lambda-repo --force
```

## Screen shots:

-  Docker:
    - ![Alt text](./images/1.jpg "Optional title1")
    - ![Alt text](./images/2.jpg "Optional title1")

-  AWS Lambda:
    - ![Alt text](./images/3.jpg "Optional title1")  

-  Deploy:
    - ![Alt text](./images/4.jpg "Optional title1")
    - ![Alt text](./images/5.jpg "Optional title1")